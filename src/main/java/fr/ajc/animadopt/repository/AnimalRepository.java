package fr.ajc.animadopt.repository;

import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import fr.ajc.animadopt.entities.AnimalEntity;



@Repository
public interface AnimalRepository extends ElasticsearchRepository<AnimalEntity,String> {

	//	@Query("{\"multi_match\" : {\"query\":\"?0\",\"type\":\"most_fields\",\"fields\":[\"espece^5\",\"sexe^5\",\"region^5\",\"commentaire\", \"histoire\", \"caractere\", \"description\"]}}}")
	//	Iterable<AnimalEntity> findByCriteres(String motsCles);


	@Query("{\"bool\": {\"should\": [{\"match\": {\"espece\":\"?0\"}},{\"match\": {\"region\": \"?1\"}},{\"match\": {\"sexe\": \"?2\"}}]}}")
	Iterable<AnimalEntity> findByCriteresR1(String espece, String region, String sexe);

	@Query("{\"bool\": {\"should\": [{\"match\": {\"espece\":\"?0\"}},{\"match\": {\"region\": \"?1\"}},{\"match\": {\"sexe\": \"?2\"}},{\"multi_match\": {\"query\": \"?3\",\"fields\": [\"commentaire^0.1\",\"histoire^0.1\",\"description^0.1\",\"caractere^0.1\"]}}]}}")
	Iterable<AnimalEntity> findByCriteresR2(String espece, String region, String sexe, String query);

	//: {\"fuzzy\" :
	//filtre chat / chien ou male / femelle apres bool
}
