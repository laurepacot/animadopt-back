package fr.ajc.animadopt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import fr.ajc.animadopt.service.AnimalService;

@SpringBootApplication
public class AnimadoptApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext appContext = SpringApplication.run(AnimadoptApplication.class, args);

		//AnimalService animalService = appContext.getBean(AnimalService.class);
		//		animalService.createDataBase();

	}
}
