package fr.ajc.animadopt.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ajc.animadopt.entities.AnimalEntity;
import fr.ajc.animadopt.repository.AnimalRepository;

@Service
public class WebScrapper {

	private final String CHIEN = "Chien";
	private final String CHAT = "Chat";

	@Autowired
	private AnimalRepository animalRepository;

	/** Boucle sur la liste d'animaux et les insèrent tous en BDD **/
	public void saveAnimalList(List<AnimalEntity> animalList) {

		System.out.println("Dans la fonction createAnimal : "+animalList.get(0));
		for (AnimalEntity animal : animalList) {
			this.animalRepository.save(animal);
		}
	}

	public void scrap30MillionsAmis() throws Exception {
		this.scrapChien30millionsDamis();
		this.scrapChat30millionsDamis();

	}

	public void scrapChien30millionsDamis() throws Exception {
		Document docChien = Jsoup.connect("https://www.30millionsdamis.fr/jagis/jadopte-un-animal/adoption/chien/")
				.get();

		List<AnimalEntity> listChien = new ArrayList<AnimalEntity>();

		int nbPages = this.calculerNbPage(docChien);

		Elements namesChiens = this.chargerElementsPage(docChien);
		List<Element> infoMemeAnimal = new ArrayList<>();
		for (Element link : namesChiens) {
			infoMemeAnimal.add(link);
			if (infoMemeAnimal.size() == 3) {
				listChien.add(this.creerAnimal(this.CHIEN, infoMemeAnimal));
				infoMemeAnimal.clear();
			}
		}

		for (int i = 2; i <= nbPages; i++) {
			docChien = Jsoup.connect(
					"https://www.30millionsdamis.fr/jagis/jadopte-un-animal/adoption/chien/?tx_ameosadoption_adoptions-list%5B%40widget_0%5D%5BcurrentPage%5D="
							+ i)
					.get();
			namesChiens = this.chargerElementsPage(docChien);
			infoMemeAnimal.clear();
			for (Element link : namesChiens) {
				infoMemeAnimal.add(link);
				if (infoMemeAnimal.size() == 3) {
					listChien.add(this.creerAnimal(this.CHIEN, infoMemeAnimal));
					infoMemeAnimal.clear();
				}
			}

		}

		// insertion en BDD
		this.saveAnimalList(listChien);

	}

	public void scrapChat30millionsDamis() throws Exception {

		Document docChat = Jsoup.connect("https://www.30millionsdamis.fr/jagis/jadopte-un-animal/adoption/chat/").get();

		List<AnimalEntity> listChat = new ArrayList<>();

		int nbPages = this.calculerNbPage(docChat);

		Elements namesChats = this.chargerElementsPage(docChat);
		List<Element> infoMemeAnimal = new ArrayList<>();
		for (Element link : namesChats) {
			infoMemeAnimal.add(link);
			if (infoMemeAnimal.size() == 3) {
				this.creerAnimal(this.CHAT, infoMemeAnimal);
				listChat.add(this.creerAnimal(this.CHAT, infoMemeAnimal));
				infoMemeAnimal.clear();
			}
		}

		for (int i = 2; i <= nbPages; i++) {
			docChat = Jsoup.connect(
					"https://www.30millionsdamis.fr/jagis/jadopte-un-animal/adoption/chat/?tx_ameosadoption_adoptions-list%5B%40widget_0%5D%5BcurrentPage%5D="
							+ i)
					.get();
			namesChats = this.chargerElementsPage(docChat);
			for (Element link : namesChats) {
				infoMemeAnimal.add(link);
				if (infoMemeAnimal.size() == 3) {
					this.creerAnimal(this.CHAT, infoMemeAnimal);
					listChat.add(this.creerAnimal(this.CHAT, infoMemeAnimal));
					infoMemeAnimal.clear();
				}
			}
		}
		// insertion en BDD
		this.saveAnimalList(listChat);
	}

	/**
	 * Méthode renvoyant le nombre de page du table de chiens/chat
	 *
	 * @param document la page web scrappée
	 * @return le nombre de pages
	 **/
	private int calculerNbPage(Document document) {
		Elements nombrePage = document.select("p[class*=pageliste-left]");
		String nbPageString = nombrePage.get(0).text();
		nbPageString = nbPageString.replaceFirst("Page 1 sur ", "");
		return Integer.valueOf(nbPageString);

	}

	/**
	 * Récupère tous les liens (balises <a href="...">) liés aux animaux
	 */
	private Elements chargerElementsPage(Document document) {
		return document.select("a[href*=/jagis/jadopte-un-animal/adopter/]");
	}

	/**
	 * Créé un animal à partir des champs fournis
	 */
	private AnimalEntity creerAnimal(String espece, List<Element> infos) {
		AnimalEntity animal = new AnimalEntity();
		animal.setEspece(espece);
		animal.setNom(infos.get(0).text());
		animal.setDescription(infos.get(1).text());
		try {
			animal.setImage("https://www.30millionsdamis.fr" + infos.get(2).child(1).child(0).attr("src"));
			// Dans certains cas, c'est le child(0) qui contient l'image...
		} catch (IndexOutOfBoundsException e) {
			animal.setImage("https://www.30millionsdamis.fr" + infos.get(2).child(0).child(0).attr("src"));
		}
		animal.setUrl("https://www.30millionsdamis.fr" + infos.get(0).attr("href"));
		this.chargerInfosSupplementairesAnimal(animal);
		System.out.println(animal);
		return animal;
	}

	private void chargerInfosSupplementairesAnimal(AnimalEntity animal) {
		try {
			// créé l'url propre à l'animal voulu
			String url = animal.getUrl();
			// Se connecte à la page de l'animal et récupère TOUT
			Document doc = Jsoup.connect(url).get();
			// Filtre le document et recupere uniquement les <p class="adoptions detail"
			Elements infos = doc.select("div[class*=adoptions detail] p");
			// A ce stade infos contient 12 lignes chacune associée à 1 élément de l'animal.
			// Le premier élément est l'age (inspecter "infos" en débug pour le savoir)
			animal.setAge(infos.get(0).text());
			// Le deuxième élément est le sexe. Et ainsi de suite...
			if (infos.get(1).text().equals("Mâle")) {
				animal.setSexe("Male");
			}
			else {
				animal.setSexe(infos.get(1).text());
			}
			animal.setSexe(infos.get(1).text());
			animal.setSterile(infos.get(2).text());

			animal.setRace(infos.get(3).text());
			animal.setNaissance(infos.get(4).text());
			animal.setTatouage(infos.get(5).text());
			animal.setRegion(infos.get(6).text());
			animal.setHistoire(infos.get(7).text());
			animal.setCaractere(infos.get(8).text());
			animal.setCommentaire(infos.get(9).text());
			// TODO : <p><a href="/refuges/refuge/5560-li-za/">LI-ZA</a><br> 135 rue de
			// l'Université - Siège Social<br> 75007 PARIS<br> Tél. : 06.08.07.11.71</p>
			animal.setRefuge(infos.get(11).text());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
