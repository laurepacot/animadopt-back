package fr.ajc.animadopt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ajc.animadopt.entities.AnimalEntity;
import fr.ajc.animadopt.entities.Queryform;
import fr.ajc.animadopt.repository.AnimalRepository;


@Service
public class AnimalService {

	@Autowired
	private AnimalRepository animalRepository;
	@Autowired
	private WebScrapper scrapper;

	public void createDataBase() {
		try {
			this.scrapper.scrap30MillionsAmis();
			// scrapper.scrapSpa();
		} catch (Exception e) {
			// TODO à logger
			System.out.println("Une erreur est survenue");
			e.printStackTrace();
		}
	}

	public Iterable<AnimalEntity> findAll() {
		return this.animalRepository.findAll();
	}

	//	public Iterable<AnimalEntity> findByCriteres(String motsCles){
	//		return this.animalRepository.findByCriteres(motsCles);
	//	}

	public Iterable<AnimalEntity> findByCriteresS(Queryform queryform){
		return this.animalRepository.findByCriteresR1(queryform.getEspece(), queryform.getRegion(), queryform.getSexe());
	}

	public Iterable<AnimalEntity> findByCriteresSq(Queryform queryform){
		return this.animalRepository.findByCriteresR2(queryform.getEspece(), queryform.getRegion(), queryform.getSexe(), queryform.getQuery());
	}




}
