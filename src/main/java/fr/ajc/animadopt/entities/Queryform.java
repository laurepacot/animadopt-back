package fr.ajc.animadopt.entities;

public class Queryform {

	private String espece;
	private String region;
	private String sexe;
	private String age;
	private String query;

	public Queryform() {}

	public Queryform(String espece, String region, String sexe, String age, String query) {
		this.espece = espece;
		this.region = region;
		this.sexe = sexe;
		this.age = age;
		this.query = query;
	}

	public Queryform(String espece, String region, String sexe, String age) {
		this.espece = espece;
		this.region = region;
		this.sexe = sexe;
		this.age = age;
	}

	public String getEspece() {
		return this.espece;
	}

	public void setEspece(String espece) {
		this.espece = espece;
	}

	public String getRegion() {
		return this.region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getSexe() {
		return this.sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public String getQuery() {
		return this.query;
	}

	public void setQuery(String query) {
		this.query = query;
	}


	public String getAge() {
		return this.age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Queryform [espece=" + this.espece + ", region=" + this.region + ", sexe=" + this.sexe + ", age=" + this.age + ", query="
				+ this.query + "]";
	}





}
