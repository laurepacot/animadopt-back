package fr.ajc.animadopt.entities;


import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName="animals",type="_doc")
public class AnimalEntity {

	@Id
	private String id;
	
	private String espece;

	private String nom;
	
	private String description;

	private String image;

	private String url;

	private String age;

	private String sexe;

	private String sterile;

	private String race;

	private String naissance;

	private String tatouage;

	private String region;

	private String histoire;

	private String caractere;

	private String commentaire;

	private String refuge;

	public AnimalEntity() {
	}



	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getEspece() {
		return espece;
	}

	public void setEspece(String espece) {
		this.espece = espece;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if (description.length() > 2000) {
			this.description = description.substring(0, 2000);
		} else {
			this.description = description;
		}
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public String getSterile() {
		return sterile;
	}

	public void setSterile(String sterile) {
		this.sterile = sterile;
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public String getNaissance() {
		return naissance;
	}

	public void setNaissance(String naissance) {
		this.naissance = naissance;
	}

	public String getTatouage() {
		return tatouage;
	}

	public void setTatouage(String tatouage) {
		this.tatouage = tatouage;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getHistoire() {
		return histoire;
	}

	public void setHistoire(String histoire) {
		if (histoire.length() > 2000) {
			this.histoire = histoire.substring(0, 2000);
		} else {
			this.histoire = histoire;
		}
	}

	public String getCaractere() {
		return caractere;
	}

	public void setCaractere(String caractere) {
		if (caractere.length() > 2000) {
			this.caractere = caractere.substring(0, 2000);
		} else {
			this.caractere = caractere;
		}
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		if (commentaire.length() > 2000) {
			this.commentaire = commentaire.substring(0, 2000);
		} else {
			this.commentaire = commentaire;
		}
	}

	public String getRefuge() {
		return refuge;
	}

	public void setRefuge(String refuge) {
		this.refuge = refuge;
	}

	@Override
	public String toString() {
		return "Animal [espece=" + espece + ", nom=" + nom + ", description=" + description + ", image=" + image
				+ ", url=" + url + ", age=" + age + ", sexe=" + sexe + ", sterile=" + sterile + ", race=" + race
				+ ", naissance=" + naissance + ", tatouage=" + tatouage + ", region=" + region + ", histoire="
				+ histoire + ", caractere=" + caractere + ", commentaire=" + commentaire + ", refuge=" + refuge + "]";
	}

}