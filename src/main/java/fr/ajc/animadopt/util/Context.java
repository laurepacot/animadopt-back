package fr.ajc.animadopt.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Context {

	private EntityManagerFactory emf = null;

	private static Context ctx = null;

	// Constructeur private pour ne pas pouvoir de créer d'objet context ailleurs
	// dans l'appli
	private Context() {
		emf = Persistence.createEntityManagerFactory("persistenceAnimadopt");
		// Seul le nom de la persistence est à changer selon les projet.
		// On peut faire un copier/coller de cette classe à chaque fois.
	}

	// pour avoir le même objet emf dans toute l'appli (emf est unique, seule et
	// unique version) = LE SINGLETON
	public static Context getInstance() {
		if (ctx == null) {
			ctx = new Context();
		}
		return ctx;
	} // si ctx n'existe pas, JPA en crée un. Sinon retourne le ctx existant. Il n'y
		// en existe qu'un seul.

	// Pour fermer emf à la fin. ctx doit être null à la fin de l'utilisation
	public void destroy() {
		if (ctx != null) {
			emf.close();
		}
		ctx = null;
	}

	public EntityManagerFactory getEmf() {
		return emf;
	}
}
