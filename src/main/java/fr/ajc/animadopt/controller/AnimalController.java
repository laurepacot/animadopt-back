package fr.ajc.animadopt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.ajc.animadopt.entities.AnimalEntity;
import fr.ajc.animadopt.entities.Queryform;
import fr.ajc.animadopt.service.AnimalService;


@RestController
public class AnimalController {

	@Autowired
	private AnimalService animalService;

	@GetMapping(value = "/api/animals")
	public Iterable<AnimalEntity> getAll(){
		return this.animalService.findAll();
	}

	//	@GetMapping(value = "/api/animals/criteres/{motsCles}")
	//	public Iterable<AnimalEntity> findByCriteres(@PathVariable String motsCles){
	//		return this.animalService.findByCriteres(motsCles);
	//	}

	@CrossOrigin(origins = "http://localhost:8100")
	@PostMapping(value = "/api/animals/criteres")
	public Iterable<AnimalEntity> findByCriteresC(@RequestBody Queryform queryform){
		System.out.println(queryform);
		return this.animalService.findByCriteresSq(queryform);
	}


}
